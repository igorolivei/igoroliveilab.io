---
title: Sobre
date: 2019-05-28
aliases:
- about-us
- contact
author:
  name: "Igor Oliveira"
---

Natalense morando em São Paulo.

Técnico em Informática pelo IFRN e graduando em Análise e Desenvolvimento de Sistemas.

Atuo área de TI há pouco mais de 7 anos; dos quais aproximadamente 4 foram como desenvolvedor (backend com uma breve passagem por frontend), e um pouco mais de 3 na implementação de práticas da cultura DevOps. Trabalhei em empresas de pequeno, médio e grande porte; além de já ter atuado com consultoria.

Atualmente procuro aprofundar cada vez mais meus conhecimentos em CI/CD, Monitoramento, Infra como Código e Cloud. Possuo experiência com diversas ferramentas e tecnologias, como Python, Git, GitLab CI, Docker, Kubernetes, Terraform, AWS, entre várias outras.

Se desejar entrar em contato, basta me enviar uma mensagem em uma de minhas redes sociais (os ícones para cada uma estão na página inicial) ou pelo e-mail: igor.bezerra96@gmail.com
