---
title: "Criando um servidor de arquivos e git com Raspberry e Ansible"
date: 2019-06-08T10:52:14-03:00
draft: false
toc: false
images:
tags:
  - raspberry
  - ansible
  - git
  - gitea
  - owncloud
---

![Raspberry stack](/images/raspberry-stack.png)

Há muito tempo sou bem atraído pelo Raspberry PI. Um minicomputador com uma capacidade incrível (para seu tamanho), um preço razoável (até no Brasil), e uma infinidade de projetos que podem ter ele como base.

Tendo em mãos:

- Um **Raspberry PI 3 Model B**
- Um **HD de 500GB** um notebook antigo em um case para HD de notebook

![Raspberry tools](/images/raspberry-tools.jpg)

E querendo estudar um pouco de **Ansible**, resolvi fazer um mini servidor de arquivos e git, sendo instalados usando Ansible. Com isso consigo ter um pouco mais de segurança e privacidade nos dados, além de uma fonte de backup disponível mesmo sem internet (ao menos quando eu estiver em casa).

Escolhi para servidor de arquivos o [ownCloud](https://owncloud.org), e para o git o [gitea](https://gitea.io); simplesmente por serem soluções leves, gratuitas e open source.

## Preparando o ambiente

No Raspberry instalei o Raspbian Stretch Lite - distro baseada no Debian - na sua versão mais nova, que nada mais é que um Debian 9. As instruções para instalação podem ser encontradas na [documentação disponível no site do Raspberry](https://www.raspberrypi.org/documentation/installation/installing-images/README.md). Todos os playbooks foram pensados para funcionarem em distros baseadas no Debian.

Instale o Ansible 2.7+ (na sua máquina, no Raspberry não precisa ;D). Se estiver usando o Debian, Ubuntu, pode seguir [esse tutorial](https://linuxconfig.org/how-to-install-ansible-on-ubuntu-18-04-bionic-beaver-linux) (a versão que tem nos repositórios padrão do Ubuntu é um pouco antiga, e vamos precisar de alguns recursos de versões mais novas). Caso esteja usando outra distro, tenho certeza que com uma rápida busca no Google você consegue encontrar como fazer.

O próximo passo é garantir nosso acesso via SSH ao Raspberry.

Primeiramente, acesse a tela de configuração do seu roteador, e descubra qual o IP do seu Raspberry (recomendo fortemente reservar um IP para ele, assim fica garantido que ao reiniciar o roteador, o IP se manterá o mesmo). Vou me referir a esse IP como `<RASPBERRY_IP>`.

![Raspberry reserved IP](/images/raspberry-reserved-ip.png)

ATENÇÃO: Antes do uso, você provavelmente vai precisar habilitar o ssh no Raspberry usando o menu do `raspi-config`.

Depois de descobrir o IP do Raspberry, vamos copiar nossa chave ssh para ele (se ainda não tiver uma, crie utilizando o comando `ssh-keygen`):

`ssh-copy-id -i ~/.ssh/<SUA_CHAVE>.pub pi@<RASPBERRY_IP>`

Valide o acesso com o comando:

`ssh pi@<RASPBERRY_IP`

O acesso não deve pedir senha.

Ok, já temos o acesso ao Raspberry, agora é hora de colocar a mão na massa! :)

Crie um diretório no seu lugar de preferência onde ficarão os playbooks, e em seguida entre nele:

`mkdir rpi_ansible`

`cd rpi_ansible`

Todos os playbooks e demais arquivos utilizador podem ser encontrados [nesse repositório](https://github.com/Igorolivei/rpi-server-ansible).

## Preparando o raspberry

Para começar as instalações, vamos criar nosso arquivo de inventário.

`vi inventory.txt`

Nele vamos definir o host onde vamos fazer nossas instalações e setar algumas variáveis que serão necessárias para os próximos playbooks. O conteúdo do meu arquivo ficou:

```ini
[raspberry_pi:vars]
ansible_user=pi
ansible_python_interpreter=/usr/bin/python3

disk=/dev/sda
owncloud_db_password=0wnCl0ud
gitea_db_password=g1te4
hostname=192.168.15.18

[raspberry_pi]
192.168.15.18
```

Para sua instalação, mude os valores para os que se adequarem ao seu caso:

- A variável `disk` deve receber o caminho do HD que você conectou, você pode descobrir isso executando um `lsblk` no Raspberry (aqui o HD apareceu como `sda`, então usarei `/dev/sda`).
- As variáveis que terminam com `db_password` contém as senhas dos bancos do Gitea e do ownCloud. Se for instalar apenas um dos dois, pode remover a que é relacionada ao outro.
- A variável `hostname` recebe o endereço do seu Raspberry. Pode ser o IP ou um hostname, caso este seja resolvido pela sua máquina (pode fazer o teste com um `ping <HOSTNAME>`).

Se quiser validar o acesso do Ansible, pode criar um playbook para pegar algumas informações:

`vi validate_conn.yml`

Com o conteúdo:

```yaml
- name: Validate connection
  hosts: raspberry-pi
  become: yes

  tasks:
  - name: Get host info
    debug:
      msg:
        - "My hostname is {{ ansible_hostname }}"
        - "My home is {{ ansible_env.HOME }}"
```

Execute com o comando `ansible-playbook validate_conn.yml -i inventory.txt` e a saída deve ser parecida com:

```
PLAY [Validate connection] ****************************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************************
ok: [192.168.15.18]

TASK [Get hostname] ***********************************************************************************************************************************
ok: [192.168.15.18] => {
    "msg": [
        "My hostname is pi-server",
        "My home is /root"
    ]
}

PLAY RECAP ********************************************************************************************************************************************
192.168.15.18              : ok=2    changed=0    unreachable=0    failed=0
```

Bom, vamos criar o primeiro playbook. Este vai:

- **Formatar e montar o disco** no caminho `/mnt/disk`;
- **Atualizar todos os pacotes** para a versão mais nova, rebootar o Raspberry e aguardar ele voltar para continuar (provavelmente o kernel também vai ser atualizado, e, confie em mim, o reboot vai evitar que vários problemas ocorram);
- Instalar o **NGINX**, que utilizaremos para servir os dois serviços, e levar sua configuração;
- Instalar o banco de dados **MariaDB** na versão 10.3 (anteriores a 10.2 geravam um bug no Gitea), o **NTP** (para garantir a data e hora no Raspberry);
- Instalar o **FirewallD** e garantir que as portas 22, 80 e 443 estejam abertas;
- Instalar o pacote **net-tools** (útil para verificar as portas usadas) e mais alguns outros pacotes que foram necessários para a instalação correta do repositório do MariaDB.

`vi prepare_machine.yml`

Ele ficou com o seguinte conteúdo:

```yaml
- name: Prepare machine
  hosts: raspberry_pi
  become: yes

  tasks:
  - name: Create mount dir
    file:
      path: /mnt/disk
      state: directory

  - name: Format volume
    filesystem:
      fstype: ext4
      dev: '{{disk}}'

  - name: Create mountpoint
    mount:
      path: /mnt/disk
      src: '{{disk}}'
      fstype: ext4
      state: mounted

  - name: Upgrade all packages to the latest version
    apt:
      name: '*'
      state: latest
      update_cache: yes
      force_apt_get: true
    notify:
      - Reboot and wait to continue

  - meta: flush_handlers

  - name: Install packages
    apt:
      name: [dirmngr, apt-transport-https, net-tools, ntp, nginx, firewalld]
      state: present

  - name: Remove default nginx config
    file:
      dest: /etc/nginx/sites-enabled/default
      state: absent

  - name: Set timezone
    timezone:
      name: America/Sao_Paulo

  - name: Ensure NTP is running and enabled as configured.
    service:
      name: ntp
      state: started
      enabled: true

  - name: Add apt-key from MariaDB
    apt_key:
      keyserver: hkp://keyserver.ubuntu.com:80
      id: '0xF1656F24C74CD1D8'
      state: present

  - name: Add MariaDB 10.3 repository
    apt_repository:
      repo: deb http://sfo1.mirrors.digitalocean.com/mariadb/repo/10.3/debian/ stretch main
      state: present
      update_cache: yes

  - name: Install MariaDB
    apt:
      name: mariadb-server
      state: present

  - name: Manage firewalld services
    firewalld:
      service: '{{item}}'
      permanent: yes
      state: enabled
      immediate: yes
    with_items:
      - ssh
      - http
      - https

  handlers:
  - name: Reboot and wait to continue
    reboot:
      reboot_timeout: 180
```

Agora que nosso playbook está pronto, basta executar:

`ansible-playbook prepare-machine.yml -i inventory.txt`

Obs.: Se preferir, pode pular as partes de execução, e executar todos os playbooks de uma vez, como eu ensino lá no final.

Você deve ver uma saída parecida com:

```

PLAY [Prepare machine] ********************************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************************
ok: [192.168.15.18]

TASK [Create mount dir] *******************************************************************************************************************************
changed: [192.168.15.18]

TASK [Format volume] **********************************************************************************************************************************
changed: [192.168.15.18]

TASK [Create mountpoint] ******************************************************************************************************************************
changed: [192.168.15.18]

TASK [Upgrade all packages to the latest version] *****************************************************************************************************
changed: [192.168.15.18]

RUNNING HANDLER [Reboot and wait to continue] *********************************************************************************************************
changed: [192.168.15.18]

TASK [Install packages] *******************************************************************************************************************************
changed: [192.168.15.18]

TASK [Remove default nginx config] ********************************************************************************************************************
changed: [192.168.15.18]

TASK [Set timezone] ***********************************************************************************************************************************
changed: [192.168.15.18]

TASK [Ensure NTP is running and enabled as configured.] ***********************************************************************************************
ok: [192.168.15.18]

TASK [Add apt-key from MariaDB] ***********************************************************************************************************************
changed: [192.168.15.18]

TASK [Add MariaDB 10.3 repository] ********************************************************************************************************************
changed: [192.168.15.18]

TASK [Install MariaDB] ********************************************************************************************************************************
changed: [192.168.15.18]

TASK [Manage firewalld services] **********************************************************************************************************************
ok: [192.168.15.18] => (item=ssh)
changed: [192.168.15.18] => (item=http)
changed: [192.168.15.18] => (item=https)

PLAY RECAP ********************************************************************************************************************************************
192.168.15.18              : ok=14   changed=12   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

## Preparando o arquivo de configuração do nginx

Eu sei que você já está ansioso para instalar as aplicações, mas antes vamos criar o arquivo de configuração do nginx, que será copiado pelos próximos playbooks (sim, ambos contém tasks de copiar, caso você queira usar só uma das aplicações):

`vi nginx-config.j2`

Com o conteúdo:

```yaml
server {
  listen 80;

  server_name {{hostname}};

  root   /var/www;
  index  index.php index.html;

  ### Gitea block ###
  location /gitea/ {
    proxy_pass http://localhost:3000/;
  }
  ### End Gitea block ###

  ### ownCloud block ###
  location /owncloud {
    # set max upload size
    client_max_body_size 512M;
    fastcgi_buffers 8 4K;                     # Please see note 1
    fastcgi_ignore_headers X-Accel-Buffering; # Please see note 2

    # Disable gzip to avoid the removal of the ETag header
    # Enabling gzip would also make your server vulnerable to BREACH
    # if no additional measures are done. See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=773332
    gzip off;

    # Uncomment if your server is build with the ngx_pagespeed module
    # This module is currently not supported.
    #pagespeed off;

    error_page 403 /owncloud/core/templates/403.php;
    error_page 404 /owncloud/core/templates/404.php;

    location /owncloud {
      rewrite ^ /owncloud/index.php$uri;
    }

    location ~ ^/owncloud/(?:build|tests|config|lib|3rdparty|templates|data)/ {
      return 404;
    }
    location ~ ^/owncloud/(?:\.|autotest|occ|issue|indie|db_|console) {
      return 404;
    }

    location ~ ^/owncloud/(?:index|remote|public|cron|core/ajax/update|status|ocs/v[12]|updater/.+|ocs-provider/.+|ocm-provider/.+|core/templates/40[34])\.php(?:$|/) {
      fastcgi_split_path_info ^(.+\.php)(/.*)$;
      include fastcgi_params;
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
      fastcgi_param SCRIPT_NAME $fastcgi_script_name; # necessary for owncloud to detect the context root https://github.com/owncloud/core/blob/v10.0.0/lib/private/AppFramework/Http/Request.php#L603
      fastcgi_param PATH_INFO $fastcgi_path_info;
      fastcgi_param modHeadersAvailable true; #Avoid sending the security headers twice
      # EXPERIMENTAL: active the following if you need to get rid of the 'index.php' in the URLs
      #fastcgi_param front_controller_active true;
      fastcgi_read_timeout 180; # increase default timeout e.g. for long running carddav/ caldav syncs with 1000+ entries
      fastcgi_pass unix:/run/php/php7.0-fpm.sock;
      fastcgi_intercept_errors on;
      fastcgi_request_buffering off; #Available since NGINX 1.7.11
    }

    location ~ ^/owncloud/(?:updater|ocs-provider|ocm-provider)(?:$|/) {
      try_files $uri $uri/ =404;
      index index.php;
    }

    # Adding the cache control header for js and css files
    # Make sure it is BELOW the PHP block
    location ~ /owncloud/.*\.(?:css|js) {
      try_files $uri /owncloud/index.php$uri$is_args$args;
      add_header Cache-Control "max-age=15778463" always;

      # Add headers to serve security related headers (It is intended to have those duplicated to the ones above)
      # The always parameter ensures that the header is set for all responses, including internally generated error responses.
      # Before enabling Strict-Transport-Security headers please read into this topic first.
      # https://www.nginx.com/blog/http-strict-transport-security-hsts-and-nginx/

      #add_header Strict-Transport-Security "max-age=15552000; includeSubDomains; preload" always;
      add_header X-Content-Type-Options nosniff always;
      add_header X-Frame-Options "SAMEORIGIN" always;
      add_header X-XSS-Protection "1; mode=block" always;
      add_header X-Robots-Tag none always;
      add_header X-Download-Options noopen always;
      add_header X-Permitted-Cross-Domain-Policies none always;
      # Optional: Don't log access to assets
      access_log off;
    }

    location ~ /owncloud/.*\.(?:svg|gif|png|html|ttf|woff|ico|jpg|jpeg|map|json) {
      try_files $uri /owncloud/index.php$uri$is_args$args;
      add_header Cache-Control "public, max-age=7200" always;
      # Optional: Don't log access to other assets
      access_log off;
    }
  }
  ### End ownCloud block ###
}
```

Caso deseje instalar apenas uma das aplicações, pode remover o bloco da outra do arquivo do nginx. Deixei comentado para facilitar a identificação de qual é qual.

Essas configurações foram baseadas nas próprias documentações do [Gitea](https://docs.gitea.io/pt-br/reverse-proxies/) e do [ownCloud](https://doc.owncloud.org/server/10.1/admin_manual/installation/nginx_configuration.html).

- O ownCloud vai poder ser acessado no endereço: `http://<RASPBERRY_IP>/owncloud`
- O Gitea vai poder ser acessado no endereço: `http://<RASPBERRY_IP>/gitea`

## Instalando o ownCloud

Para a instalação do ownCloud, me baseei [nesse post](https://fuga.cloud/academy/tutorials/deploy-owncloud-with-ansible/).

Vamos criar nosso playbook:

`vi install_owncloud.yml`

Com o seguinte conteúdo:

```yaml
- name: Install ownCloud
  hosts: raspberry_pi
  become: yes

  tasks:
  - name: Create ownCloud dir
    file:
      path: /var/www/owncloud
      state: directory

  - name: Create data dir on disk
    file:
      path: /mnt/disk/owncloud_data
      state: directory
      owner: www-data
      group: www-data

  - name: Create symlink to data dir on mounted disk
    file:
      src: /mnt/disk/owncloud_data
      dest: /var/www/owncloud/data
      state: link
      owner: www-data
      group: www-data

  - name: Install required packages for ownCloud
    apt:
      name: [openssl, php-imagick, php7.0-common, php7.0-cgi, php7.0-curl, php7.0-fpm, php7.0-gd, php7.0-imap, php7.0-intl, php7.0-json, php7.0-ldap, php7.0-mbstring, php7.0-mcrypt, php7.0-mysql, php7.0-pgsql, php-smbclient, php-ssh2, php7.0-sqlite3, php7.0-xml, php7.0-zip, python3-mysqldb, python3-dev]
      state: present

  - name: Add apt-key from ownCloud
    apt_key:
      url: https://download.owncloud.org/download/repositories/10.2.0/Debian_9.0/Release.key
      state: present

  - name: Add ownCloud repository
    apt_repository:
      repo: deb https://download.owncloud.org/download/repositories/10.2.0/Debian_9.0/ /
      state: present
      update_cache: yes

  - name: Install / Download ownCloud
    apt:
      name: owncloud-files

  - name: Create 'owncloud' Database
    mysql_db:
      name: owncloud
      state: present
    notify:
      - Change password of owncloud database user

  - name: Copy owncloud nginx config
    template:
      src: nginx-conf.j2
      dest: /etc/nginx/conf.d/rpi-server
    notify:
      - Enable nginx config
      - Restart nginx

  handlers:
  - name: Change password of owncloud database user
    command: >
      mysql -u root --execute="GRANT ALL ON owncloud.* to 'owncloud'@'localhost' IDENTIFIED BY '{{owncloud_db_password}}'; FLUSH PRIVILEGES;"

  - name: Enable nginx config
    file:
      src: /etc/nginx/conf.d/rpi-server
      dest: /etc/nginx/sites-enabled/rpi-server
      state: link
      owner: www-data
      group: www-data

  - name: Restart nginx
    systemd:
      name: nginx
      state: reloaded
```

Esse playbook cria os diretórios, instala pacotes e o owncloud, garante que a configuração do nginx esteja lá, e este seja reiniciado.

Execute a instalação com:

`ansible-playbook install_owncloud.yml -i inventory.txt`

Você deverá ver uma saída parecida com:

```
PLAY [Install ownCloud] *******************************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************************
ok: [192.168.15.18]

TASK [Create ownCloud dir] ****************************************************************************************************************************
changed: [192.168.15.18]

TASK [Create data dir on disk] ************************************************************************************************************************
changed: [192.168.15.18]

TASK [Create symlink to data dir on mounted disk] *****************************************************************************************************
changed: [192.168.15.18]

TASK [Install required packages for ownCloud] *********************************************************************************************************
changed: [192.168.15.18]

TASK [Add apt-key from ownCloud] **********************************************************************************************************************
changed: [192.168.15.18]

TASK [Add ownCloud repository] ************************************************************************************************************************
changed: [192.168.15.18]

TASK [Install / Download ownCloud] ********************************************************************************************************************
changed: [192.168.15.18]

TASK [Create 'owncloud' Database] *********************************************************************************************************************
changed: [192.168.15.18]

TASK [Copy owncloud nginx config] *********************************************************************************************************************
changed: [192.168.15.18]

RUNNING HANDLER [Change password of owncloud database user] *******************************************************************************************
changed: [192.168.15.18]

RUNNING HANDLER [Enable nginx config] *****************************************************************************************************************
changed: [192.168.15.18]

RUNNING HANDLER [Restart nginx] ***********************************************************************************************************************
changed: [192.168.15.18]

PLAY RECAP ********************************************************************************************************************************************
192.168.15.18              : ok=13   changed=12   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

### Concluindo a instalação do OwnCloud

Após instalar, abra seu navegador e acesse `http://<RASPBERRY_IP>/owncloud`. Estamos nos últimos passos da configuração do ownCloud. Vamos preencher os campos:

- Defina um usuário administrador e sua senha
- Logo abaixo, em "Armazenamento e Banco de Dados", selecione `MySQL/MariaDB` e preencha os dados necessários:
  - Usuário: `owncloud`
  - Senha: A que você definiu para o banco do ownCloud (no inventory.txt que coloquei acima era `0wnCl0ud`)
  - Database: `owncloud`
  - Clique em `Concluir configuração` e pronto!

Seu servidor de arquivos está pronto para uso! :)

## Instalando o Gitea

Para a instalação do Gitea, me baseei [nesse tutorial de instalação](https://www.vultr.com/docs/how-to-install-gitea-on-debian-9).

Para começar, vamos criar o template do arquivo de serviço do Gitea (relaxa, o Ansible lida com o resto):

`vi gitea-service.j2`

Com o conteúdo:

```
[Unit]
Description=Gitea (Git with a cup of tea)
After=syslog.target
After=network.target
After=mariadb.service

[Service]
# Modify these two values and uncomment them if you have
# repos with lots of files and get an HTTP error 500 because
# of that
###
#LimitMEMLOCK=infinity
#LimitNOFILE=65535
RestartSec=2s
Type=simple
User=gitea
Group=gitea
WorkingDirectory=/var/lib/gitea/
ExecStart=/usr/local/bin/gitea web -c /etc/gitea/app.ini
Restart=always
Environment=USER=gitea HOME=/home/gitea GITEA_WORK_DIR=/var/lib/gitea
# If you want to bind Gitea to a port below 1024 uncomment
# the two values below
###
#CapabilityBoundingSet=CAP_NET_BIND_SERVICE
#AmbientCapabilities=CAP_NET_BIND_SERVICE

[Install]
WantedBy=multi-user.target
```

Agora vamos ao playbook:

`vi install_gitea.yml`

O conteúdo do playbook ficou:

```yaml
- name: Install gitea
  hosts: raspberry_pi
  become: yes

  tasks:
  - name: create gitea group
    group:
      name: gitea
      state: present

  - name: create gitea user
    user:
      name: gitea
      group: gitea
      home: /home/gitea
      comment: Gitea
      system: yes
      shell: /bin/bash

  - name: Create gitea dir on disk
    file:
      path: '{{ item }}'
      state: directory
      owner: gitea
      group: gitea
      mode: 0750
    with_items:
      - /mnt/disk/gitea_repositories
      - /mnt/disk/gitea_lfs

  - name: Create gitea etc dir
    file:
      path: /etc/gitea
      state: directory
      owner: root
      group: gitea
      mode: 0770

  - name: Create gitea directories
    file:
      path: '{{ item }}'
      state: directory
      owner: gitea
      group: gitea
    with_items:
      - /var/lib/gitea/custom
      - /var/lib/gitea/public

  - name: Create gitea 0750 directories
    file:
      path: '{{ item }}'
      state: directory
      owner: gitea
      group: gitea
      mode: 0750
    with_items:
      - /var/lib/gitea/indexers
      - /var/lib/gitea/log
      - /var/lib/gitea/data

  - name: Create symlink of repositories to data dir on mounted disk
    file:
      src: /mnt/disk/gitea_repositories
      dest: /var/lib/gitea/data/repositories
      state: link
      owner: gitea
      group: gitea

  - name: Create symlink of lfs to data dir on mounted disk
    file:
      src: /mnt/disk/gitea_lfs
      dest: /var/lib/gitea/data/lfs
      state: link
      owner: gitea
      group: gitea

  - name: Install git
    apt:
      name: git
      state: present

  - name: Create 'gitea' Database
    mysql_db:
      name: gitea
      state: present
    notify:
      - Change password of gitea database user

  - name: Download gitea binary
    get_url:
      url: https://dl.gitea.io/gitea/1.7.6/gitea-1.7.6-linux-arm-7
      dest: /usr/local/bin/gitea
      owner: gitea
      group: gitea
      mode: 0755

  - name: Copy gitea service file
    template:
      src: gitea-service.j2
      dest: /etc/systemd/system/gitea.service
    notify:
      - Start service gitea
      - Add first lines to app.ini

  - name: Copy gitea nginx config
    template:
      src: nginx-conf.j2
      dest: /etc/nginx/conf.d/rpi-server
    notify:
      - Enable nginx config
      - Restart nginx

  handlers:
  - name: Change password of gitea database user
    command: >
      mysql -u root --execute="GRANT ALL ON gitea.* TO 'gitea'@'localhost' IDENTIFIED BY '{{gitea_db_password}}' WITH GRANT OPTION; FLUSH PRIVILEGES;"

  - name: Start service gitea
    systemd:
      name: gitea
      state: restarted
      daemon_reload: yes
      enabled: yes

  - name: Add first lines to app.ini
    lineinfile:
      path: /etc/gitea/app.ini
      line: '{{item}}'
    with_items:
      - '[server]'
      - 'ROOT_URL=http://{{hostname}}/gitea/'
      - 'LFS_CONTENT_PATH=/var/lib/gitea/data/lfs'
      - '[repository]'
      - 'ROOT=/var/lib/gitea/data/repositories'
    notify:
      - Start service gitea

  - name: Enable nginx config
    file:
      src: /etc/nginx/conf.d/rpi-server
      dest: /etc/nginx/sites-enabled/rpi-server
      state: link
      owner: www-data
      group: www-data

  - name: Restart nginx
    systemd:
      name: nginx
      state: reloaded
```

Execute com o comando:

`ansible-playbook install-gitea.yml -i inventory.yml`

E a saída deve ser algo parecido com:

```
PLAY [Install gitea] **********************************************************************************************************************************

TASK [Gathering Facts] ********************************************************************************************************************************
ok: [192.168.15.18]

TASK [create gitea group] *****************************************************************************************************************************
changed: [192.168.15.18]

TASK [create gitea user] ******************************************************************************************************************************
changed: [192.168.15.18]

TASK [Create gitea dir on disk] ***********************************************************************************************************************
changed: [192.168.15.18] => (item=/mnt/disk/gitea_repositories)
changed: [192.168.15.18] => (item=/mnt/disk/gitea_lfs)

TASK [Create gitea etc dir] ***************************************************************************************************************************
changed: [192.168.15.18]

TASK [Create gitea directories] ***********************************************************************************************************************
changed: [192.168.15.18] => (item=/var/lib/gitea/custom)
changed: [192.168.15.18] => (item=/var/lib/gitea/public)

TASK [Create gitea 0750 directories] ******************************************************************************************************************
changed: [192.168.15.18] => (item=/var/lib/gitea/indexers)
changed: [192.168.15.18] => (item=/var/lib/gitea/log)
changed: [192.168.15.18] => (item=/var/lib/gitea/data)

TASK [Create symlink of repositories to data dir on mounted disk] *************************************************************************************
changed: [192.168.15.18]

TASK [Create symlink of lfs to data dir on mounted disk] **********************************************************************************************
changed: [192.168.15.18]

TASK [Install git] ************************************************************************************************************************************
changed: [192.168.15.18]

TASK [Create 'gitea' Database] ************************************************************************************************************************
changed: [192.168.15.18]

TASK [Download gitea binary] **************************************************************************************************************************
changed: [192.168.15.18]

TASK [Copy gitea service file] ************************************************************************************************************************
changed: [192.168.15.18]

TASK [Copy gitea nginx config] ************************************************************************************************************************
ok: [192.168.15.18]

RUNNING HANDLER [Change password of gitea database user] **********************************************************************************************
changed: [192.168.15.18]

RUNNING HANDLER [Start service gitea] *****************************************************************************************************************
changed: [192.168.15.18]

RUNNING HANDLER [Add first lines to app.ini] **********************************************************************************************************
changed: [192.168.15.18] => (item=[server])
changed: [192.168.15.18] => (item=ROOT_URL=http://192.168.15.18/gitea/)
changed: [192.168.15.18] => (item=LFS_CONTENT_PATH=/var/lib/gitea/data/lfs)
changed: [192.168.15.18] => (item=[repository])
changed: [192.168.15.18] => (item=ROOT=/var/lib/gitea/data/repositories)

RUNNING HANDLER [Start service gitea] *****************************************************************************************************************
changed: [192.168.15.18]

PLAY RECAP ********************************************************************************************************************************************
192.168.15.18              : ok=18   changed=16   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

### Concluindo a instalação do Gitea

Para concluir a instalação, acesse `http://<RASPBERRY_IP>/gitea/install`, e preencha os campos necessários:

  - Configure a conexão ao banco:
    - Senha: A senha definida no inventory.txt (no exemplo dado, utilizei `g1te4`)
  - Opcionalmente crie uma conta de administrador; do contrário, a conta utilizada de administrador será a primeira conta que for criada
  - Clique em "Instalar Gitea", e pronto!

Servidor git pronto para uso! :)

## Executando todos os playbooks de uma vez

Se preferir, pode criar todos os arquivos e playbooks, e executar com `ansible-playbook prepare-machine.yml install_owncloud.yml install_gitea.yml -i inventory.txt`.

## Resultado

Aqui está meu projetinho pronto (sim, eu sei que preciso de um case pro Raspberry, e eu sei que é meio irônico usar uma caixa de Orange PI como case temporário [O Orange PI vai ser outro projeto]):

![Raspberry done](/images/raspberry-done.jpg)

Só lembrando que tudo foi pensado para um uso pessoal, e o Raspberry de maneira nenhuma vai servir como um servidor de produção.

Lembrando também que agradeço qualquer dica ou sugestão, principalmente relacionadas aos playbooks, já que estes foram os meus primeiros.
