---
title: "Da zona de conforto ao aprendizado contínuo"
date: 2019-05-30
author:
  name: "Igor Oliveira"
type:
- post
- posts
tags:
- aprendizado continuo
- zona de conforto
- three ways
weight: 10
---

## Introdução

Na última semana estive no DevOpsDays Porto Alegre apresentando uma palestra com o título **"Da zona de conforto ao aprendizado contínuo e como as three ways podem te ajudar nesse processo"**. A motivação para começar a palestrar em eventos parte justamente da minha motivação em sair da minha zona de conforto, uma vez que sempre tive um grande medo de falar em público e, por isso, sempre evitei fazê-lo.

A ideia aqui é transcrever mais ou menos o que falei lá e acrescentar mais algumas coisas que vim pensando depois; aproveitando para começar a prática de escrever e compartilhar mais meu conhecimento, e, consequentemente, aprender mais.

Os slides da palestra estão [aqui](https://speakerdeck.com/igorolivei/da-zona-de-conforto-ao-aprendizado-continuo).

## A zona de conforto

A zona de conforto é algo que pode estar presente em qualquer área da sua vida, mas minha intenção é focar mais na parte profissional.

Minha motivação em falar mais sobre isso foi uma conversa com um colega que veio me falar que se sente muito estagnado, e gostaria de "mudar"; daí comecei a falar algumas coisas da minha (curta) experiência e que podem vir a ser úteis para outras pessoas.

A zona de conforto nada mais é do que uma série de pensamentos, atitudes e comportamentos que te fazem se sentir seguro, não te trazem nenhum medo, estresse, ansiedade ou sustos, e que te fazem manter uma rotina e padrão de comportamento; afinal, você não quer que nada mude.

Aproveito pra dizer que estar na zona de conforto não é algo necessariamente ruim, desde que esteja alinhado com seus objetivos (o que não acontece em boa parte dos casos).

Normalmente quando se está na zona de conforto, você não sai do lugar. Fica exatamente realizando as mesmas atividades e consequentemente não evolui. No meu último emprego eu estava assim. Parte por minha culpa, parte por culpa do ambiente em que eu trabalhava mesmo, que não demandava de mim nenhum esforço novo.

Aqui vão alguns motivos pra sair da zona de conforto:

- A sensação de segurança quando se está na zona de conforto é **falsa**, uma vez que algo pode acontecer (como você ser demitido, por exemplo) e você vai ser forçado a sair da sua zona de conforto, o que geraria um estresse muito maior que resolver sair por conta própria
- Quando você sai da zona de conforto sua produtividade aumenta, já que você consegue estudar no seu próprio ritmo
- Consequentemente você aprende a lidar com o inesperado, já que está mais acostumado a lidar com coisas novas
- Conhecer mais coisas significa ter um aumento na criatividade
- Você descobre novos talentos quando aprende mais, e eu sou uma prova disso: no último ano parei de trabalhar com desenvolvimento web e passei a trabalhar como Consultor DevOps, depois de conhecer novas tecnologias que me permitiram entrar mais nessa área
- [A estabilidade desliga seu cérebro](https://epocanegocios.globo.com/Carreira/noticia/2018/10/quer-aprender-algo-novo-saia-da-sua-zona-de-conforto.html)

Percebi o quão negativo estava sendo para mim estar na zona de conforto depois que resolvi sair do emprego em que estava. Não ter chance e motivação para evoluir (não que a falta de motivação fosse culpa do emprego) foi um dos motivos para ter me demitido, mas não o principal. De toda forma, aí vão alguns sintomas de que você está na zona de conforto (de maneira negativa):

- Você mantém um desempenho constante, não evolui nem aprende nada novo nunca
- Já faz um tempo que você precisou (ou quis mesmo) aprender alguma coisa nova
- A ideia de alguma mudança que vá além de trocar de sala sempre te assusta, afinal você não está preparado para isso
- Você sempre reclama do seu trabalho, das tarefas que faz, mas não faz absolutamente nada para mudar isso
- Acordar de manhã e lembrar que tem que trabalhar é sempre uma tortura (e os finais de semana são um alívio)
- Sua rotina não está alinhada com o que você quer para sua vida

### Estou na zona de conforto, e agora?

Só senti mesmo o impacto de estar na zona de conforto quando comecei a procurar um novo emprego. No primeiro momento eu me senti realmente desesperado:

- A cada vaga nova que eu via tinha vontade de pedir para voltar para o emprego anterior
- Não me sentia nem um pouco capaz quando via as qualificações necessárias de cada vaga, tinha a sensação (ou melhor, tinha certeza!) de que não sabia de nada
- Nesse momento a [**síndrome do impostor**](https://pt.wikipedia.org/wiki/S%C3%ADndrome_do_impostor) tende a se intensificar, e eu passei a me questionar como consegui meu emprego anterior, já que depois de mais de 3 anos trabalhando na área, aparentemente, eu não servia para nenhuma outra empresa
- Tentei começar a estudar de imediato, mas tinha tanta coisa que eu precisava aprender, que me senti totalmente perdido
- Comecei a dar desculpas para mim mesmo, e isso é bem comum. Quem nunca disse que não tinha tempo para estudar, e meia hora depois estava maratonando uma série?

Percebi que seria melhor parar um pouco, dar uma respirada, me acalmar e seguir em frente. Aqui você pode:

- **Definir qual seu objetivo**, mesmo que não seja nada tão específico; na época eu resolvi que queria ser um desenvolvedor full stack (minha experiência era basicamente com backend)
- **Pesquisar o que é necessário para chegar onde quer**, procurar vagas, se basear em alguém que você tem como referência, acompanhar as tendências do mercado, etc; eu tive muito interesse em uma vaga numa empresa que desenvolve ferramentas utilizando realidade aumentada, e comecei a me dedicar no que eles pediam (spoiler: não consegui a vaga)
- **Criar pequenos objetivos a curto prazo**, porque eles vão te ajudar a ver que é possível sim aprender bem rápido, e isso vai te manter mais motivado
- **Fazer uma lista de prioridades**; por exemplo: se preciso aprender 3 novas ferramentas (linguagem de programação, banco de dados, etc), quais delas vou precisar mais a curto prazo? Na época eu comecei a estudar mais intensamente frontend, e paralelamente me dediquei a aprender um framework JavaScript do qual eu nunca tinha ouvido falar antes

## O aprendizado contínuo

Decidir sair da zona de conforto não me fez conseguir a vaga que eu tanto queria, mas me fez conseguir ver os benefícios de manter uma rotina de aprendizado contínuo (e ainda me proporcionou um emprego numa empresa excelente e trabalhar com profissionais incríveis).

Antes de tudo, **"aprendizado contínuo"** é simplesmente manter uma constante atualização e aquisição de conhecimentos. E aqui vão alguns motivos para seguir uma rotina de aprendizado contínuo:

- É extremamente importante nos dias de hoje, e ainda mais na área de TI, que está em constante atualização
- Saber mais significa um aumento na habilidade de resolver problemas; é simples: sabendo mais você tem a capacidade de atuar em tarefas mais variadas
- Segundo Jonas Prising, CEO da ManpowerGroup, ["o modelo de um emprego para a vida toda está morto"](https://www.linkedin.com/pulse/job-life-model-dead-heres-what-millennials-need-know-jonas-prising/) (recomendo muito fazer uma pausa para essa leitura)
- Uma tendência que vejo é que cada vez mais as empresas buscam por um profissional "resolvedor de problemas"; ou seja, um profissional que tem a capacidade lógica de - não exatamente resolver qualquer problema, mas ter uma noção do que se trata e saber direcionar para a área ou pessoa certa; isso está bem ligado a capacidade de um profissional "T", que tem um conhecimento mais aprofundado numa área específica (a linha vertical do "T") e pelo menos um conhecimento básico em diversas outras (a linha horizontal do "T")

Uma vez que resolver manter uma rotina de aprendizado contínuo, é hora de começar a estudar. Aqui vão algumas dicas:

- **Experimente diversas formas de estudar** e encontre a que se encaixa melhor para você. Eu, por exemplo, não gosto de videoaulas (enquando vejo bastante gente que estuda principalmente assim) porque não consigo me manter focado em vídeos. No meu caso, ler a documentação do que estiver estudando, fazer anotações (no papel mesmo) e exercícios práticos, me ajuda a fixar melhor
- **Pratique bastante**, essa é a melhor forma de fixar o que aprendeu
- **Compartilhe seu conhecimento e esteja disposto a ajudar outras pessoas**. Sempre que me disponho a ajudar alguém, pesquiso bastante para garantir que não vou passar nenhuma informação errada, e sempre acabo aprendendo algo novo; tanto com as pesquisas quanto com a pessoa que estou ajudando
- **Defina metas**. Para mim o ideal é ter uma meta maior, a longo prazo, e outras menores que eu consiga alcançar logo. Aqui dá até para usar alguma metodologia como o [OKR](https://rockcontent.com/blog/okr/) ou qualquer outra que você se sinta mais confortável
- **Crie uma rotina de estudos**. Comece a estudar diariamente ou na frequência que puder. Uma pesquisa revelou que [a gente leva em média 66 dias para criar um novo hábito](https://www.spring.org.uk/2009/09/how-long-to-form-a-habit.php) e este realmente fazer parte da nossa rotina
- **Estude pelo menos um minuto por dia**. Essa é a ideia do [Kaizen e a prática de um minuto diário](https://iheartintelligence.com/overcome-laziness/) para evitar a procrastinação. A ideia é que um minuto é muito pouco, então é possível praticar diariamente, e ao mesmo tempo é tão pouco que a gente sempre vai querer passar um pouco mais de tempo
- **Mantenha o foco no seu objetivo**. Não adianta querer abraçar o mundo, fazer várias coisas ao mesmo tempo e não conseguir se aprofundar em nada

### As three ways do DevOps e o aprendizado contínuo

A gente passa basicamente 8 horas por dia, pelo menos 5 dias por semana pensando no como, e agindo para entregar valor aos nossos clientes. Mas muitas vezes negligenciamos o bem valioso que é nosso próprio conhecimento. Da mesma forma que as [Three Ways do DevOps](https://itrevolution.com/the-three-ways-principles-underpinning-devops/) nos ajudam a entregar valor para nossos clientes, elas podem nos ajudar a ter uma rotina melhor de estudos.

Aqui vão algumas lições que podemos tirar delas:

#### First Way: Princípio do fluxo

- **Visualize o trabalho** - Visualizar o trabalho te ajuda a se planejar melhor, e até a não se esquecer. Gosto de acompanhar minhas tarefas (não só as relacionadas a estudo) com um pequeno [Kanban](https://blog.runrun.it/o-que-e-kanban/)
- **Evite muitas mudanças de contexto** - Ficar mudando de tarefas ou se distraindo constamente atrapalha sua concentração. No momento em que estiver estudando tente focar somente nisso. Coloque o celular no silencioso, saia das redes sociais, e evite conversas paralelas
- **Limite os WIP** (*Work in progress*) - Quanto mais coisas você estiver estudando paralelamente, maior vai ser o tempo para "concluir" algo (ou alcançar o nível desejado). Isso é bem relacionado ao item anteior. Tente limitar a uma quantidade que não te atrapalhe. E não existe número mágico, então o ideal é ir experimentando e ajustar da maneira que fique mais confortável para você

#### Second Way: Feedback contínuo

- **Pratique e faça auto avaliações constantemente** - Se avaliar é muito necessário, já que essa é a melhor maneira de notar sua evolução. Faça testes e atividades práticas com frequência, ou peça para alguém com mais experiência te ajudar nesse ponto
- **Se avaliar permite que você encontre seus pontos fracos** - Se avaliar te ajuda a encontrar seus pontos mais fracos, e esses não devem ser ignorados. Pelo contrário, aproveite para focar no que você tem mais dificuldade. Eu, por exemplo, tenho uma certa dificuldade em aprender coisas relacionadas a redes de computadores; então, ao invés de evitar, procuro estar sempre estudando algo sobre isso
- **Meça sua evolução** - Manter uma visualização do trabalho, como falado anteriormente, também te ajuda a avaliar sua evolução. Com certeza acompanhar de maneira mais visível o quanto você já fez vai te manter motivado
- **Se encontrar alguma "falha", faça um "swarming"** - É comum que estudando alguma coisa, a gente encontre um ponto relacionado a outro conhecimento que não temos. Nesse momento, o melhor a se fazer é parar um pouco, e entender pelo menos o básico desse outro conhecimento para evitar que isso atrapalhe o avanço dos seus estudos

#### Third Way: Experimentação contínua e aprendizado

- **Não existe falha, só aprendizado** - Uma coisa que a cultura DevOps nos encoraja muito a praticar é que nossos erros sirvam para aprendizado e melhoria. Quando algo der errado, aprenda com o ocorrido e use isso para melhorar o que estiver fazendo, ao invés de se sentir culpado
- **Se algo der errado, experimente uma nova maneira de fazer** - Como falei anteriormente em alguns pontos, não existe uma fórmula mágica para como ou o que estudar. Por isso, experimentar novas maneiras de fazer devem ser parte do nosso dia-a-dia. Só assim conseguimos encontrar o que funciona melhor para nós
- **Repetição e prática são pré-requisitos para dominar qualquer coisa**
- **Se arrisque** - Nunca tenha medo de se arriscar. Pelo contrário: procure fazer isso. Aceite novos desafios e projetos, experimente estudar coisas de outras áreas, e procure sempre coisas novas; afinal, essa prática ajuda nosso cérebro a ter mais facilidade em assimilar novas informações

## Dicas finais

- **Ninguém é melhor que ninguém** - É comum admirarmos pessoas, e acharmos que elas são "seres superiores", mas isso não é verdade. É possível sim chegar (e até passar!) ao nível delas. Na maioria das vezes elas só tem mais tempo de experiência ou tiveram mais oportunidades que nós
- **Nunca é tarde para começar** - Não existe "tarde" para começar uma mudança. Temos vários exemplos de pessoas que começaram uma faculdade aos 70 ou 80 anos; então não deixe que essa ideia te impeça de começar ou te desmotive
- **Respeite seu tempo e seu ritmo** - Mais importante de tudo: cada um tem seu ritmo, seus problemas e seu tempo. Manter uma rotina de aprendizado contínuo não deve ser algo exaustivo ou que te cause estresse. Não se cobre demais, e procure sempre manter um equilíbrio entre os estudos e o lazer. Isso é essencial para ter uma boa qualidade de vida!
